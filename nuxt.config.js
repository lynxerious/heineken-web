import { resolve } from "path";

const env = process.env.NODE_ENV;
const envFile = `.env.${env}`;

require("dotenv").config({ path: resolve(__dirname, envFile) });

export default {
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  mode: "spa",
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: "static",
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    title: "Heineken - Rinh bia về nhà. Tiền nạp thả ga.",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: "Rinh bia về nhà. Tiền nạp thả ga."
      },
      {
        property: "og:url",
        content: "https://rinhbiavenha.com"
      },
      {
        property: "og:type",
        content: "website"
      },
      {
        property: "og:title",
        content: "Rinh Bia Về Nhà"
      },
      {
        property: "og:description",
        content: "https://rinhbiavenha.vn"
      },
      {
        property: "og:image",
        content: "https://i.imgur.com/r6CISD9.png"
      }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/beercap.png?v=7" },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css2?family=Roboto:wght@500;600&display=swap"
      },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css2?family=Open+Sans:wght@800&display=swap"
      }
    ],
    script: [
      { src: "https://tracking.mcom.app/public/lib/mcom-tracking.js" },
      { src: "https://sp.zalo.me/plugins/sdk.js", body: true },
      { src: "https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/7.10.4/polyfill.min.js", body: true }
      // {
      //   id: "fb-script",
      //   async: "",
      //   defer: "",
      //   crossorigin: "anonymous",
      //   nonce: "divKLkNP",
      //   src: "https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v8.0&appId=143366482876596&autoLogAppEvents=1",
      //   body: true
      // }
    ]
  },

  // env: {
  //   API: process.env.API
  // },
  /*
   ** Global CSS
   */
  css: ["assets/scss/_styles.scss"],
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: [
    // "~/plugins/filters.ts",
    // "~/plugins/globals.ts",
    // "~/plugins/vee-validate.ts",
    // { src: '~plugins/v-calendar.ts', ssr: false }
  ],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: ["@nuxt/typescript-build"],
  /*
   ** Nuxt.js modules
   */
  modules: [
    "@nuxtjs/style-resources",
    // "@nuxtjs/apollo",
    // "bootstrap-vue/nuxt",
    // "@nuxtjs/firebase",
    "@nuxtjs/toast"
    // "@nuxtjs/axios"
  ],
  styleResources: {
    scss: ["assets/scss/_mixins.scss"]
  },
  // apollo: {
  //   errorHandler: "~/apollo/error-handler.ts",
  //   clientConfigs: {
  //     default: "~/apollo/config.ts"
  //   }
  // },
  // bootstrapVue: {
  //   bootstrapCSS: false,
  //   bootstrapVueCSS: false,
  //   componentPlugins: ["ModalPlugin", "DropdownPlugin"]
  // },
  // firebase: {
  //   config: {
  //     apiKey: "AIzaSyD1w7GLfzJPrIEwuxlB8YZnRzCk_HxCrBw",
  //     authDomain: "hworld-f764d.firebaseapp.com",
  //     databaseURL: "https://hworld-f764d.firebaseio.com",
  //     projectId: "hworld-f764d",
  //     storageBucket: "hworld-f764d.appspot.com",
  //     messagingSenderId: "717408487183",
  //     appId: "1:717408487183:web:f368c3a3c1df5d6b17207a",
  //     measurementId: "G-VE74WW94K1"
  //   },
  //   services: {
  //     auth: {
  //       persistence: "local",
  //       ssr: false
  //     }
  //   }
  // },
  toast: {
    position: "top-center",
    register: [
      {
        name: "error",
        message: message => message,
        options: {
          theme: "toasted-primary",
          duration: 5000,
          className: "toast-danger"
        }
      },
      {
        name: "success",
        message: message => message,
        options: {
          theme: "toasted-primary",
          duration: 5000,
          className: "toast-success"
        }
      },
      {
        name: "default",
        message: message => message,
        options: {
          theme: "toasted-primary",
          duration: 5000,
          className: "toast-default"
        }
      },
      {
        name: "info",
        message: message => message,
        options: {
          theme: "toasted-primary",
          duration: 5000,
          className: "toast-info"
        }
      }
    ]
  },
  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  // build: {
  //   transpile: ["vee-validate/dist/rules"]
  // },
  srcDir: "src/",
  server: {
    port: 8080, // default: 3000
    host: "0.0.0.0" // default: localhost
  },
  loading: {
    color: "#00843A",
    height: "3px"
  },
  generate: {
    dir: "firebase/public"
  }
};
